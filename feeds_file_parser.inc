<?php

/**
 * @file
 * File metadata parser plugin.
 */

/**
 * Feeds parser plugin that parses file metadata.
 */
class feeds_file_parser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $items = array();
    
    $file_path = $fetcher_result->getFilePath();
    
    $basename 	= basename($file_path);
    $path	= drupal_dirname($file_path);
    $parent 	= array_pop(split("[/\]", $path));
    
    
    $items[] = array(
	'name' 		=> $basename,
	'uri' 		=> $file_path,
	'dirname'	=> $parent,
    );

    $path = drupal_get_path('module', 'feeds_directory_parser');

    $result = new FeedsParserResult($items);
    return $result;
  }

  /**
   * Return mapping sources.
   */
  public function getMappingSources() {
    return array(
      'name' => array(
        'name' => t('File name'),
        'description' => t('Name of the file.'),
      ),
      'uri' => array(
        'name' => t('File URI'),
        'description' => t('URI of the file.'),
      ),
      'dirname' => array(
        'name' => t('Directory name'),
        'description' => t('Name of the parent directory.'),
      ),
    ) + parent::getMappingSources();
  }
}
